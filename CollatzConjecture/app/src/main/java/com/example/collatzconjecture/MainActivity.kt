package com.example.collatzconjecture

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Find the GUI elements by ID using the Ressources
        val calculateBtn = findViewById<Button>(R.id.calculateBtn)
        val result = findViewById<TextView>(R.id.result)
        val impostor = findViewById<TextView>(R.id.impostor)
        val highestNum = findViewById<TextView>(R.id.highestNum)

        //Create a StringBuilder object to append the sequences of numbers
        val sb = StringBuilder()

        //Load the data saved in SharedPreferences
        loadData()

        //Add an event to the Calculate button
        calculateBtn.setOnClickListener {
            //Always reset the String Builder so it doesn't repeat append
            sb.clear()
            //Find userInput and parse it to Int
            val userInput = findViewById<EditText>(R.id.userInput).text.toString().toInt()
            //Show a toast if the userInput is incorrect (lower than 1)
            if (userInput < 1) {
                Toast.makeText(this, R.string.invalid_input, Toast.LENGTH_SHORT).show()
            }
            //If user input is valid, call the calculateCollatzConjecture method to calculate and print the result
            else {
                result.text = userInput.toString()
                calculateCollatzConjecture(
                    userInput,
                    highestNum,
                    sb,
                    result,
                    impostor
                )
            }
        }
    }

    //Method to calculate and print the Collatz Conjecture
    private fun calculateCollatzConjecture(
        userNumber: Int,
        highestNum: TextView,
        sb: StringBuilder,
        result: TextView,
        impostor: TextView
    ) {
        //Find the userInput and parse it to Int
        var userInput = findViewById<EditText>(R.id.userInput).text.toString().toInt()
        //If userInput is greater than the current highest number, calculate the conjecture and update the highest number
        if (userInput > highestNum.text.toString().toInt()) {
            while (userInput != 1) {
                if (userInput % 2 == 0) {
                    userInput /= 2
                } else if (userInput % 2 == 1) {
                    userInput = (userInput * 3) + 1
                }
                //Not possible but if a number somehow violates the conjecture...
                else {
                    impostor.text =
                        String.format(getString(R.string.violates_conjecture, userNumber))
                }
                //Append the result to the StringBuilder object
                sb.append(", ").append(userInput).toString()
            }
            //Update highest number
            highestNum.text = userNumber.toString()
            //Save data permanently
            saveData(userNumber)
        }
        //If userInput is not greater than the current highest number, only calculate the conjecture
        else {
            while (userInput != 1) {
                if (userInput % 2 == 0) {
                    userInput /= 2
                    sb.append(", ").append(userInput).toString()
                } else if (userInput % 2 == 1) {
                    userInput = (userInput * 3) + 1
                    sb.append(", ").append(userInput).toString()
                }
                //Not possible but if a number somehow violates the conjecture...
                else {
                    impostor.text =
                        String.format(getString(R.string.violates_conjecture, userNumber))
                }
            }
        }
        //Append the whole String Builder to the result
        result.append(sb)
    }

    //Save data using sharedPreferences permanently in the background
    private fun saveData(
        userNumber: Int,
    ) {
        val highestNumberSaver = userNumber.toString()
        highestNum.text = highestNumberSaver
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply {
            putString("STRING_KEY", highestNumberSaver)
        }.apply()
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show()
    }

    //Load the data saved in the background
    private fun loadData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val savedString = sharedPreferences.getString("STRING_KEY", 0.toString())
        highestNum.text = savedString
    }
}
